//sammy baker
//hw09 nov 25 2018
//cse 2 


import java.util.Scanner;
import java.util.Arrays;
public class CSE2Linear{
  public static void main (String[]args){
    System.out.println("Enter 15 ints: ");
    Scanner input = new Scanner(System.in);
    int count = 0;
    int[] arrayOne = new int[15];
    while (count < 15){
      if(input.hasNextInt()){
        arrayOne[count] = input.nextInt();
        if(arrayOne[count] < 0 || arrayOne[count] > 100){
          System.out.println("You were not in the range");
          System.exit(0);
        }
       else{
         if (count > 0){
           if (arrayOne[count] < arrayOne[count - 1]){
             System.out.println("You didnt enter an integer that was greater than the last one");
             System.exit(0);            
           }
         }
         count++;
       } 
      }
      else{
        System.out.println("You didnt enter an integer");
        System.exit(0);
      }
    }
   System.out.println(Arrays.toString(arrayOne));
   System.out.print("choose a grade to search for: ");
    if (input.hasNextInt()){
      int x = input.nextInt();
      BinarySearch(arrayOne, x);
      Scramble(arrayOne);
      System.out.println("Scrambled: ");
      System.out.println(Arrays.toString(arrayOne));
      System.out.print("Enter a grade to search for: ");
      if (input.hasNextInt()){
        LinearSearch(arrayOne,x);
      }
      else{
        System.out.println("You didnt enter an integer");
        System.exit(0);
      }
    }
    else{
      System.out.println("You didnt enter a grade to search for");
      System.exit(0);
    }
  }
public static void LinearSearch(int[]arrayOne, int x){
  for (int i = 0; i < arrayOne.length; i++){
    if (arrayOne[i] == x){
      System.out.println(x + " was found in the list in " + (i + 1) + " iterations");
      break;
    }
    else if (arrayOne[i] != x && i == (arrayOne.length - 1)){
      System.out.println(x + "was not found in " + (i + 1) + "iterations");
    }
  }
}
public static int[] Scramble(int[]arrayOne){
  for (int i = 0; i < arrayOne.length; i++){
    int x = (int)(Math.random()*arrayOne.length);
    int hold = arrayOne[i];
    while (x != i){
      arrayOne[i]=arrayOne[x];
      arrayOne[x]=hold;
      break;
    }
  }
  return arrayOne;
}
public static void BinarySearch(int[]arrayTwo, int number){
  int high = arrayTwo.length - 1;
  int low = 0;
  int count = 0;
  while (low <= high){
    count++;
    int middle = (high + low)/2;
    if (number < arrayTwo[middle]){
      high = middle - 1;
    }
    else if (number > arrayTwo[middle]){
      low = middle + 1;
    }
    else if (number == arrayTwo[middle]){
      System.out.println(number + " was found in " + count + " iterations");
      break;
    }
  }
  if(low > high){
    System.out.println(number + " was not found in " + count + " iterations");
  }
}  
}

