/// sammy baker 
////// lab 2
////CSE 002 Cyclometer
//// sept 6th 2018
/// purpose of the program is to record the time spent and the number of tire roations on a bike
////
public class Cyclometer {
  // main method:
public static void main(String[] args) {
    // input data:
    int secsTrip1 = 480; // seconds in trip 1
    int secsTrip2 = 3220; // seconds in trip 2
    int countsTrip1 = 1561; // number of tire rotations in trip 1
    int countsTrip2 = 9037; // number of tire rotations in trip 2
    // variables:
    double wheelDiameter = 27.0; // tire diameter
    double PI = 3.14159; // pi
    double feetPerMile = 5280; // feet in a mile
    double inchesPerFoot = 12; // inches in a foot
    double secondsPerMinute = 60; // seconds in a minute
    // output data:
    double distanceTrip1, distanceTrip2, totalDistance; // creating variables as doubles
    // print trip info statements:
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // info statement for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // info statement for trip 2
    // calculations for distances:
    distanceTrip1 = countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; // distance for trip 1 changed into miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // distance for trip 2 changed into miles
    totalDistance = distanceTrip1+distanceTrip2; // total distance of both trips in miles
    // printing distances of each trip
    System.out.println("Trip 1 was "+distanceTrip1+" miles."); // distance of trip 1 in miles
    System.out.println("Trip 2 was "+distanceTrip2+" miles."); // distance of trip 2 in miles
    System.out.println("THe total distance was "+totalDistance+" miles."); // total distance in miles
  } // end of main method
} // end of class