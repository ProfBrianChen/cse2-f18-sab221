////sammy baker
///CSE 02 lab 08
///nov 8, 2018
/// arrays ///

import java.util.Random;
public class Arrays {
    public static void main(String[] args) {
      final int numVar = 100;
      int [] string1 = new int[numVar];
      int [] string2 = new int[numVar];
      int [] count = new int[numVar];
      int ranNum;
      System.out.print("array one holds the following numbers: ");
      for (int i = 0; i < numVar; i++){
        ranNum = (int)(Math.random()*99);
        string1[i] = ranNum;
        System.out.print(string1[i] + " ");
      }
      System.out.println();
      for (int j = 0; j < numVar; j++){
        string2[string1[j]] += 1;
      }
      for(int k = 0; k < numVar; k++){
        System.out.println(k + " occurs " + string2[k] + " times.");
      }
    }
  }
