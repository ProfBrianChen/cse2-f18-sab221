////sammy baker
///CSE 02 hw 03
///sept 16, 2018
/// by giving the dimensions of a pyramid the program calculates the volume of the pyramid ///

// scanner class:
import java.util.Scanner;
// public class:
public class Pyramid { 
  // main method:
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in ); // tells the program that you are inputing something
    // input:
    System.out.print("The length of the square side of the pyramid is (input length): "); // print statement telling user to put in the length of the square side of the pyramid 
    double lengthSide = myScanner.nextDouble(); // stores length of side in double 
    System.out.print("The height of the pyramid is (input height): "); // print statement telling user to put in the height of the pyramid
    double heightPyramid = myScanner.nextDouble(); // stores height in double 
    // calculations:
    double volumePyramid = Math.pow(lengthSide, 2); // finds the area of the base
    volumePyramid *= heightPyramid; // multiplys the area off the base times the height
    volumePyramid /= 3; // calculates the volume of the pyramid and stores it in double
    // output:
    System.out.println("The volume inside the pyramid is: " + volumePyramid); // prints the volume of the pyramid
    
  } // end main method
} // end of class