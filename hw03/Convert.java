////sammy baker
///CSE 02 hw 03
///sept 16, 2018
/// converts inches over acres to cubic miles of rain ///

// scanner class:
import java.util.Scanner;
// public class:
public class Convert { 
  // main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // tells the program that you are inputing something
    // input:
    System.out.print("Enter the affected area in acres: "); // print statement telling user to put in number of acres
    double acreAmount = myScanner.nextDouble(); // stores number of acres in double 
    System.out.print("Enter the rainfall in the affected area: "); // print statement telling user to put in how much rainfall there was
    double rainfallAmount = myScanner.nextDouble(); // stores amount of rainfall in double 
    // calculations:
    double rainGallons = acreAmount*rainfallAmount*27154; // converts acres and inches into gallons
    double cubicMiles = rainGallons/1101117130711.3; // converts gallons to cubic miles
    // output
    System.out.println(cubicMiles + " cubic miles"); // prints the amount of cubic miles
    
  } // end of main method
}  // end of class