////sammy baker
///CSE 02 hw 05
///oct 8, 2018
/// computes the probability of each hand in poker ///

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class PatternD {
// Begin main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Establish to scanner that you are taking inpu
    // Input:
    System.out.print("Input integer (1-10): "); // Print statement asks user to input an integer
    
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junk = myScanner.next(); 
      System.out.print("Invalid input. Input integer 1-10: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    int input = myScanner.nextInt();
    while(input < 1 || input > 10) {
      System.out.print("Invalid input. Input integer 1-10: ");
      input = new Scanner(System.in).nextInt();
    }
    int r = input;
    for(int p=r;p>=1;--p){
      for(int k=p;k>0;--k){ //increasing each level printing
        System.out.print(k+ " ");
      }	
      System.out.println(" ");
    }
}
}