////sammy baker
///CSE 02 lab 07
///oct 25, 2018
// makes random sentences

import java.util.Random;
import java.util.Scanner;

public class lab07 {
	public static String firstString() {
		Random randomGenerator = new Random();
		int randomInt1 = randomGenerator.nextInt(10);
		String randWord = "<>";
    switch(randomInt1) {
      case 0: randWord = "fast";
              break;
      case 1: randWord = "quick";
              break;
      case 2: randWord = "slow";
              break;
      case 3: randWord = "big";
              break;
      case 4: randWord = "small";
              break;
      case 5: randWord = "old";
              break;
      case 6: randWord = "young";
              break;
      case 7: randWord = "smart";
              break;
      case 8: randWord = "dumb";
              break;
      case 9: randWord = "funny";
              break;
                                
    }
    return randWord;
  }
  	public static String secondString() {
		Random randomGenerator = new Random();
		int randomInt2 = randomGenerator.nextInt(10);
		String randWord = "<>";
    switch(randomInt2) {
      case 0: randWord = "boy";
              break;
      case 1: randWord = "girl";
              break;
      case 2: randWord = "mom";
              break;
      case 3: randWord = "dad";
              break;
      case 4: randWord = "cat";
              break;
      case 5: randWord = "dog";
              break;
      case 6: randWord = "woman";
              break;
      case 7: randWord = "man";
              break;
      case 8: randWord = "baby";
              break;
      case 9: randWord = "fish";
              break;
                                
    }
    return randWord;
  }
  	public static String thirdString() {
		Random randomGenerator = new Random();
		int randomInt3 = randomGenerator.nextInt(10);
		String randWord = "<>";
    switch(randomInt3) {
      case 0: randWord = "bit";
              break;
      case 1: randWord = "ate";
              break;
      case 2: randWord = "slapped";
              break;
      case 3: randWord = "kissed";
              break;
      case 4: randWord = "loved";
              break;
      case 5: randWord = "hated";
              break;
      case 6: randWord = "left";
              break;
      case 7: randWord = "passed";
              break;
      case 8: randWord = "hit";
              break;
      case 9: randWord = "took";
              break;
                                
    }
    return randWord;
  }
  	public static String fourthString() {
		Random randomGenerator = new Random();
		int randomInt4 = randomGenerator.nextInt(10);
		String randWord = "<>";
    switch(randomInt4) {
      case 0: randWord = "rock";
              break;
      case 1: randWord = "pig";
              break;
      case 2: randWord = "bus";
              break;
      case 3: randWord = "person";
              break;
      case 4: randWord = "bug";
              break;
      case 5: randWord = "pool";
              break;
      case 6: randWord = "coffee";
              break;
      case 7: randWord = "food";
              break;
      case 8: randWord = "house";
              break;
      case 9: randWord = "cookie";
              break;
                                
    }
    return randWord;
  }
  public static void main (String[]args){
    Scanner myScanner = new Scanner(System.in);
    String randWord1 = firstString();
    String subject = secondString();
    String randWord3 = thirdString();
    String randWord4 = fourthString();
    System.out.println("The " + randWord1 + " " + subject + " " + randWord3 + " the " + randWord4 + ".");
    System.out.print("do you want a new sentence? Type 1 for yes or 0 for no. ");
		int sent = myScanner.nextInt();
		if (sent == 1) {
			//main(new String[] {});
      Random randomGenerator = new Random();
		int randomInt5 = randomGenerator.nextInt(4);
    switch(randomInt5) {
      case 0: 
        System.out.println("This " + subject + " " + randWord3 + " to be with the  " + randWord4 + ".");
              break;
      case 1: 
        System.out.println("It used the " + randWord4 + " to have fun.");
              break;
      case 2: 
        System.out.println("Also this " + subject + " " + randWord3 + " other things too.");
              break;
      case 3: 
        System.out.println("Many " + subject + "s " + randWord3 + " " + randWord4 + "s" + ".");
              break;
    }
      main(new String[]  {});
		}
		else {
			return;
		}
	}
}