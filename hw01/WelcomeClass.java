//////
///// Sammy Baker
///// 9/2
////CSE 02 Welcome Class
////
public class WelcomeClass{
  
  public static void main(String args[]){
    //prints the welcome graphic to terminal window
    System.out.println("  ------------");
    System.out.println("   | WELCOME |");
    System.out.println("  ------------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--A--B--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I am Sammy Baker. I am a Sophomore at Lehigh and I am on the water polo team here");
  }
  
}