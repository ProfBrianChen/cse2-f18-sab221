////// sammy baker
////CSE2 hw02
////Arithmetic Calculations
/////sept 9 2018 
public class Arithmetic {
  // main method:
public static void main(String[] args) {
    // input variables:
    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; //cost per pair of pants
    int numShirts = 2; // number of swearshirts
    double shirtPrice = 24.99; // cost per shirt
    int numBelts = 1; // number of belts
    double beltPrice = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate
    // output variables:
    double costPants, costShirts, costBelts; // stating cost variables as doubles
    // calculations, cost of each item group:
    costPants = numPants*pantsPrice; // total cost of pants
    costShirts = numShirts*shirtPrice; // toal cost of shirts
    costBelts = numBelts*beltPrice; // total cost of belts
    // multiple cost by 100
    costPants = costPants*100; // pants
    costShirts = costShirts*100; // shirts
    costBelts = costBelts*100; // belts
    // convert into integer
    int costPantss = (int) costPants; // pants
    int costShirtss = (int) costShirts; // shirts
    int costBeltss = (int) costBelts; // belts
    // divide by 100 and print values:
    System.out.println("The cost of pants before sales tax is $"+(costPantss/100.0)); // pants
    System.out.println("The cost of shirts before sales tax is $"+(costShirtss/100.0)); // shirts
    System.out.println("The cost of belts before sales tax is $"+(costBeltss/100.0)); // belts
    // calculations and print statements:
    double salesTaxPants = costPantss*paSalesTax; // sales tax on pants
    double salesTaxShirts = costShirtss*paSalesTax; // sales tax on shirts 
    double salesTaxBelts = costBeltss*paSalesTax; // sales tax on belts 
    int salesTaxPantss = (int) salesTaxPants; // convert pants value into int
    int salesTaxShirtss = (int) salesTaxShirts; // convert shirts value into int
    int salesTaxBeltss = (int) salesTaxBelts; // convert belts value into int
    System.out.println("The cost of sales tax for pants is $"+(salesTaxPantss/100.0)); // pants
    System.out.println("The cost of sales tax for shirts is $"+(salesTaxShirtss/100.0)); // shirts
    System.out.println("The cost of sales tax for belts is $"+(salesTaxBeltss/100.0)); // belts
    // calculations and print statements, totals:
    double costNoTax = costPants+costShirts+costBelts; // total cost of purchases before tax
    double totalTax = salesTaxPants+salesTaxShirts+salesTaxBelts; // total sales tax
    double totalCost = costNoTax+totalTax; // total paid for transaction
    int costNoTax1 = (int) costNoTax; // convert to int
    int totalTax1 = (int) totalTax; // convert to int
    int totalCost1 = (int) totalCost; // convert to int
    System.out.println("The total cost before tax is $"+costNoTax1/100.0); // print price before tax
    System.out.println("The total cost of sales tax is $"+totalTax1/100.0); // print price of tax
    System.out.println("The total cost of the transaction is $"+totalCost1/100.0); // print total price
  } // end main method
} // end class arithmetic