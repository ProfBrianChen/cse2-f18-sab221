////sammy baker
///CSE 02 hw 06
///oct 22, 2018
/// computes the probability of each hand in poker ///

// Import scanner class:
import java.util.Scanner;
// Begin class:
public class EncryptedX {
// Begin main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Establish to scanner that you are taking inpu
    // Input:
    System.out.print("Input integer (0-100): "); // Print statement asks user to input an integer
    
    while (!myScanner.hasNextInt() ) { // Checks to see if the input is valid
      String junk = myScanner.next(); 
      System.out.print("Invalid input. Input integer 0-100: "); // Print statement asks user to input an integer because their previous one was not valid
    }
    int input = myScanner.nextInt();
    while(input < 0 || input >= 100) {
      System.out.print("Invalid input. Input integer 0-100: ");
      input = new Scanner(System.in).nextInt();
    }
    
    for (int i = 0; i < input; i++){
      for (int j = 0; j < input; j++){
        if (i==j || j == (input - (i+1))){
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}