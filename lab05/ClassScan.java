////sammy baker
///CSE 02 lab 05
///oct 4, 2018
// loops that ask the user about a class that they are taking
import java.util.Scanner; // import scanner class

public class ClassScan { // begin class
  public static void main(String[] args) { // begin main method
  Scanner myScanner = new Scanner(System.in); // where you are taking input from
    // all of the input
    // first, course number:
    System.out.print("enter the course number: "); // asks to input an integer
    while (!myScanner.hasNextInt() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string 
      System.out.print("error with your input. input an integer for the course number: "); // asks user to input an integer
    }
    int courseNumber = myScanner.nextInt(); // stores input 
    String blank = myScanner.nextLine(); // next input will be read as a line
    // second, department name:
    System.out.print("enter the department name: "); // asks user for department name
    while (!myScanner.hasNextLine() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string
      System.out.print("error with your input. input a string for the department name: "); // asks user to input a string 
    }
    String departName = myScanner.nextLine(); // stores input 
    // third, number of times a week you have the class:
    System.out.print("enter how many times a week the class meets: "); //  asks user for number of times it meets
    while (!myScanner.hasNextInt() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string
      System.out.print("error with your input. input an integer for the number of times the class meets a week: "); // asks user to input an integer
    }
    int amountMeet = myScanner.nextInt(); // stores input
    String blank2 = myScanner.nextLine(); // next input will be raed as a line
    // fourth, the time the class starts:
    System.out.print("enter the time the class meets in the form 00:00 AM or 00:00 PM: "); // asks user for the time that the class starts
    while (!myScanner.hasNextLine() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string
      System.out.print("error with your input. input the correct form of what time the class starts: "); // asks user to input in the right form
    }
    String startTime = myScanner.nextLine(); // stores input
    // fourth, the instructors name:
    System.out.print("enter the instructor's name: "); // asks user for name of instructor
    while (!myScanner.hasNextLine() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string
      System.out.print("error with your input. input a string for name of your instructor: "); // asks user to input a string 
    }
    String instructorsName = myScanner.next(); // stores input
    // fifth, the number of students in the class:
    System.out.print("enter the number of students in the class: "); // asks user for number of students
    while (!myScanner.hasNextInt() ) { // checks to see if the input is okay
      String junk = myScanner.next(); // makes the invalid input into a string
      System.out.print("error with your input. input an integer for number of students in the class: "); // asks user to input an integer
    }
    int numberStudents = myScanner.nextInt(); // stores input
    
    //output: prints everythimng
    System.out.println("course Number: " + courseNumber);
    System.out.println("department: " + departName);
    System.out.println("times per week: " + amountMeet);
    System.out.println("start time: " + startTime);
    System.out.println("instructor: " + instructorsName);
    System.out.println("number of students: " + numberStudents);
    
  } // End main method
} // End class