////sammy baker
///CSE 02 hw 07
///oct 30, 2018
/// does different operations to words in a paragraph ///

import java.util.Scanner;
public class wordTools {
public static Scanner scanner = new Scanner(System.in);
////////
  public static void printMenu(){
    System.out.println("\nMENU");
    System.out.println("c -- number of non-whitespace characters");
    System.out.println("w -- number of words");
    System.out.println("f -- find text");
    System.out.println("r -- replace all !'s");
    System.out.println("s -- shorten spaces");
    System.out.println("q -- quit");
    System.out.println("\nchoose an option: "); 
  }
  ///////////
  public static int getNumOfNonWSCharacters(String text) {
    text = text.trim().replaceAll("\\s","");
    return text.length();
  }
  /////////
  public static int getNumOfWords(String text){
    text = shortenSpace(text);
    String[] words = text.split(" ");
    return words.length;
  }
  //////////
  public static int findText(String text, String find){
    int count1 = 0;
    int i = 0;
    
    while((i = text.indexOf(find))!= -1){
      text = text.substring(i + find.length());
      count1 += 1;
    }
    return count1;
  }
  /////////
  public static String replaceExclamation(String text){
    String place = text.replaceAll("!",".");
    return place;
  }
  /////////
  public static String shortenSpace(String text){
    String place = text.trim().replaceAll(" +"," ");
    return place;
  }
  //////////
  public static void main(String[]args){
      System.out.println("Enter a sample text:");
      String text = scanner.nextLine();
      System.out.println("You wrote: " + text);
      printMenu();
      char choices = scanner.nextLine().charAt(0);
      
      switch (choices){          
        case 'q':
          System.exit(0);  
        case 'c':
          int countnonWhite = getNumOfNonWSCharacters(text);
          System.out.println("number of non white space characters: "+ countnonWhite);
          break;          
        case 'w':
          int countWords = getNumOfWords(text);
          System.out.println("number of words: "+ countWords);
          break;          
        case 'f':
          System.out.println("Enter a word or phrase to be found: ");
          String find = scanner.nextLine();
          int countFinder = findText(text,find);
          System.out.println("\"" + find + "\" instances: " + countFinder);
          break;          
        case 'r':
          String string2 = replaceExclamation(text);
          System.out.println("edited text: " + string2);
          break;         
        case 's':
          string2 = shortenSpace(text);
          System.out.println("edited text: "+ string2);
          break;          
        default:
          System.out.println("Input error. Try again.");        
      }
      System.out.println();
    }
  }


  
  
  
  
  
  
  
  
  