//// Sammy Baker
/// Lab04
////sept 20, 2018
///card generator: picks random card from a deck of standard playing cards

//begin class:
public class CardGenerator {
  //main method:
  public static void main(String[] args) {
    // initialize variables:
    int randomNumber; // variable where random number is stored
    String suitofCard; // variable where the suit of the card is stored
    String valueofCard = "card"; // variable where the value of the card is stored
    // random number generator:
    randomNumber = (int) (Math.random()* 52) + 1; // picks a random number from 1 to 52 demonstrating the different cards
    // pick a suit:
    if (randomNumber < 14) {
      suitofCard = "diamonds";} // says that if the random number is less than 14 then the suit is diamonds
    else if (14 <= randomNumber && randomNumber <= 26) {
      suitofCard = "clubs";} // says that if the random number is 14-26 then the suit is clubs
    else if (27 <= randomNumber && randomNumber <= 39) {
      suitofCard = "hearts";} // says that if the random number is 27-39 then the suit is hearts
    else {
      suitofCard = "spades";} // says that if the random number is 40-52 then the suit is spades
    // picks card value:
    int cardNumber = (randomNumber%13); // remainder of the random number when divded by 13
    switch (cardNumber) { // switch card number to value of card 
      case 1: valueofCard = "ace";
        break;
      case 2: valueofCard = "2";
        break;
      case 3: valueofCard = "3";
        break;
      case 4: valueofCard = "4";
        break;
      case 5: valueofCard = "5";
        break;
      case 6: valueofCard = "6";
        break;
      case 7: valueofCard = "7";
        break;
      case 8: valueofCard = "8";
        break;
      case 9: valueofCard = "9";
        break;
      case 10: valueofCard = "10";
        break;
      case 11: valueofCard = "jack";
        break;
      case 12: valueofCard = "queen";
        break;
      case 13: valueofCard = "king";
        break;
    } // end switch statement
    
    // print statement to show suit and value of random card:
    System.out.println("You picked the " + valueofCard + " of " + suitofCard +""); // Prints statement for user
  } // end Main Method
} // end Class