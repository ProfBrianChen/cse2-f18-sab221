////sammy baker
///CSE 02 Lab 03
///sept 13, 2018
/// check: reads bill value, calculates necessary tip, and splits bill evenly between people ///

// scanner class:
import java.util.Scanner;
// public class:
public class Check { 
  // main method:
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // tells the program that you are inputing something
    // input:
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // print statement telling user to put in cost of the check
    double checkCost = myScanner.nextDouble(); // stores cost of check in double 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // print statement telling user to put in how much tip they want to give
    double tipPercent = myScanner.nextDouble(); // stores tip percentage in double 
    tipPercent /= 100; // converts tip percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); // print statement telling user to put in how many people went to dinner to split the check that many ways
    int numPeople = myScanner.nextInt(); // stores number of people in an integer
    // calculations:
    double totalCost, costPerPerson; // makes these variables doubles
    int dollars, dimes, pennies; // makes variables to store value of the cost
    totalCost = checkCost*(1 + tipPercent); // calculate the total cost of the check with tip
    costPerPerson = totalCost/numPeople; // calculate the cost per person of the check with tip
    dollars = (int) costPerPerson; // whole cost per person, without the decimal
    dimes = (int) (costPerPerson*10) % 10; // the amount of dimes
    pennies = (int) (costPerPerson*100) % 10; // the amount of pennies
    // output:
    System.out.println("Each person in the group owes $"+ dollars + "." + dimes + pennies); // how much each person has to pay
    
  } // end of main method
} // end of class